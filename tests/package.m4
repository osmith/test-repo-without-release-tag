# Signature of the current package.
m4_define([AT_PACKAGE_NAME],
  [osmo-mgw])
m4_define([AT_PACKAGE_TARNAME],
  [osmo-mgw])
m4_define([AT_PACKAGE_VERSION],
  [1.9.0.25-a02f])
m4_define([AT_PACKAGE_STRING],
  [osmo-mgw 1.9.0.25-a02f])
m4_define([AT_PACKAGE_BUGREPORT],
  [openbsc@lists.osmocom.org])
m4_define([AT_PACKAGE_URL],
  [])
